import java.util.LinkedList;

public class BallGame {
	
	BallPool bp;
	LinkedList<Integer> delay = new LinkedList<Integer>();
	
	public BallGame(BallPool pool) {
		bp = pool;
	}

	public int playGame(int turns, int startScore) {
		
		while(turns > 0) {
			//1. draw
			Ball b = bp.draw();
			//2. action
			Class<? extends Ball> c = b.getClass();
			if(delay.peek() == null || delay.peekFirst() != turns) {
				switch(c.toString() ) {
				case "class BlueBall":
					startScore -= b.getSticker();
					bp.add(new BlueBall());
					break;
				case "class RedBall":
					startScore *= 2;
					bp.draw();
					break;
				case "class PinkBall":
					startScore *= 2;
					bp.draw();
					bp.add(new GelbBall());
					break;
				case "class GreenBall":
					startScore += b.getSticker();
					break;
				case "class GelbBall":
					startScore *= b.getSticker();
					startScore++;
					delay.add(turns-3);
					break;
				default:
					System.out.println("unknown draw");
				}
			} else {
				if(delay.peek() != null) {
					delay.removeFirst(); 
				}
			}
			//3. increase
			b.setSticker(b.getSticker()+1);
			//4. add
			bp.add(b);
			//turn over
			turns--;
		}
		
		return startScore;
	}

	/*
	 * unused atm
	 */
	
	private int labGame() {
		Ball b = new Ball();
		if(b instanceof BlueBall) {
			b.getSticker();
		} else if(b instanceof GreenBall) {
			
		} else if(b instanceof PinkBall) {
			
		} else if(b instanceof RedBall) {
			
		} else { //assume ball yellow
			
		}
		return 0;
	}
}
